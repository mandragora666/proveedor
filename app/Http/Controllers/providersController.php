<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\Product;

class providersController extends Controller
{

    public function index(Request $request){

              $providers = Provider::all();
            return view('providers.index',['providers'=>$providers]);
        }


        public function create()
        {
            $providers = Provider::all();

            return view('providers.create',['providers'=>$providers]);

        }


        public function store(Request $request)
        {


            $provider = new Provider();

                //'name' -> $inputs['name'],
                //'address' -> $inputs['address'],
               //'location' -> $inputs['location'],
                //'phone' -> $inputs['phone'],
                  // 'email' -> $inputs['email'],
            //$provider()->save();
            //return redirect('providers');

            $provider->name=request('name');
            $provider->location=request('location');
            $provider->phone=request('phone');
            $provider->email=request('email');
            $provider()->save();
            return response()->json($provider);
            //return redirect('providers');
           //return response()->json($provider);
          //  return response()->json($provider);




        }



        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $provider= Provider::find($id);
            return view('providers.edit',['provider'=>$provider]);
        }


        public function update(Request $request, $id)
        {
            //
        }


        public
        function destroy($id)
        {
            //
        }
    }


