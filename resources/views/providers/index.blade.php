<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Usuarios</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <h1>Lista de provedores</h1>
    <div class="row float-right" style="margin-bottom: 10px;">
        <a href="/providers/create" class="btn btn-primary">
            Crear provedor
        </a>
    </div>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Nombres</th>
            <th>Dirección</th>
            <th>Pais</th>
            <th>Telefono</th>
            <th>Email</th>
            <th></th>
            <th></th>
        </tr>
        @foreach($providers as $pr)
            <tr>
                <td>{{$pr->id}}</td>
                <td>{{$pr->name}}</td>
                <td>{{$pr->address}}</td>
                <td>{{$pr->location}}</td>
                <td>{{$pr->phone}}</td>
                <td>{{$pr->email}}</td>

                <td><a href="/providers/{{$pr->id}}/edit" class="btn btn-warning">
                        Editar
                    </a></td>
                <td>
                    <form action="/providers/{{$pr->id}}" method="post">
                        @method('DELETE')
                        @csrf
                        <input type="submit" name="eliminar" class="btn btn-danger" value="Eliminar">
                    </form>

            </tr>
        @endforeach
    </table>
</div>
</body>
</html>

